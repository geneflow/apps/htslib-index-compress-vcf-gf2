HTSLib BGZip and Tabix GeneFlow App
===================================

Version: 1.10.2-02

This GeneFlow app wraps HTSLib bgzip and tabix tools to compress and index a VCF file. 

Inputs
------

1. input: Input VCF File

Parameters
----------

1. output: Output Directory - The name of the output directory that will contain compressed VCF and index files. 
